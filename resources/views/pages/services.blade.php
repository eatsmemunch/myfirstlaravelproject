@extends('layouts.app')

@section('content')
    <h1 class="display-3">Services</h1>
    <p class="h4" style="padding-bottom: 20px;">This is the services section</p>

    @if(count($services) > 0)
        @foreach($services as $service)
            <li class="alert alert-secondary h5" style="margin: 0;"> {{$service}} </li>
        @endforeach
    @endif

@endsection