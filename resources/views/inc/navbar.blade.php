<nav class="navbar navbar-dark navbar-expand-lg navbar-fixed-top bg-dark">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">EXAMPLE-APP</a>
		</div>

		<button type="button" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#navbarContent" aria-expanded="false" aria-controls="navbar" aria-label="Toggle navigation">
			<!-- sr-only doesn't work in Bootstrap 4 and above so I replaced it with the 'aria label="Toggle navigation"' in the above tag

			<span class="sr-only">Toggle navigation</span>

			-->
			<span class="navbar-toggler-icon"></span>
		</button>

		<div id="navbarContent" class="collapse navbar-collapse">

			<ul class="nav navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item">
					<a class="nav-link active" href="/">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="/about">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="/services">Services</a>
				</li>
			</ul>

		</div><!--/.nav-collapse -->
	</div>
</nav>