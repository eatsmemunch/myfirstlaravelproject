<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<h1> Sample Page loaded by the controller </h1>
	<p> {{$front_end}} </p>

	@if(count($topics) > 0)
		@foreach($topics as $topic)
			<li> {{$topic}} </li>
		@endforeach
	@endif

</body>
</html>